
const inquirer = require('inquirer');

console.log('Welcome. Here\'s my super generic node thingie.');

function run() {

    let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    const mapping = numbers.map(x => x * 2);

    console.log('Numbers = ' + numbers + '  -  And after forEach * 2 : ' + mapping)

    setTimeout(function () {
        console.log('Now for a real question after waiting two seconds');

        let questions = [{
            type: 'input',
            name: 'hobby',
            message: 'What is your favorite thing to do?'
        },]
        inquirer.prompt(questions).then(answers => {
            console.log(`So you like to ${answers.hobby} huh! `);
            console.log('_______ ********** ________');
            console.log('ok, ok, one more...');

            setTimeout(function () {

                console.log('Cuáles son las vacas más perezosas?')
                setTimeout(function () {
                    console.log('Vacaciones! 🤣  Bahahahahahahha!!! I don\'t get it. 🤷‍')
                }, 3000); // end timeout 3rd

            }, 2000); // end timeout 2nd
        })  // end inquirer prompt

    }, 2000); // end 1st timeout

    console.log('Console.log after setTimeout function just because');

    return;

}

run();